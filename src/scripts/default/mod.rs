//
// This file echoes a bunch of color codes to the 
// terminal to demonstrate what's available.  Each 
// line is the color code of one forground color,
// out of 17 (default + 16 escapes), followed by a 
// test use of that color on all nine background 
// colors (default + 8 escapes).
//

const ESC: &str = "[";
const HEADER: &str = "\n                 40m        41m        42m        43m        44m        45m        46m        47m";
const T: &str = "gYw";
const FGS: &[&str] = &[
    "    m", "   1m", "  30m", "1;30m",
    "  31m", "1;31m", "  32m", "1;32m",
    "  33m", "1;33m", "  34m", "1;34m",
    "  35m", "1;35m", "  36m", "1;36m",
    "  37m", "1;37m",
];
const BGS: &[&str] = &[
    "40m", "41m", "42m", "43m",
    "44m", "45m", "46m", "47m",
];

pub struct Default {}

impl crate::scripts::Script for Default {

    fn get_name(&self) -> String {
        "Default".to_string()
    }

    fn exec(&self) {
        println!("{}", HEADER);

        for fg in FGS {
            print!(" {} {}{}  {}", fg, ESC, fg, T);

            for bg in BGS {
                print!("  {}{}{}{}   {}   {}0m", ESC, fg, ESC, bg, T, ESC);
            }
            println!();
        }
        println!();
    }

}