// ANSI color scheme script by pfh
// Source: http://crunchbang.org/forums/viewtopic.php?pid=139126#p139126
// Initializing mod by lolilolicon from Archlinux
//

const ESC: &str = "[";

pub struct Bars {}

impl crate::scripts::Script for Bars {
    fn get_name(&self) -> String {
        "Bars".to_string()
    }

    fn exec(&self) {
        for b in 0..2 {
            let bld = if b == 1 { format!("{}1m", ESC) } else { String::new()  };
            for i in 1..7 {
                print!("{}{}3{}m▬▬▬▬▬ ", bld, ESC, i);
            }
            println!();
        }
        println!("{}0m", ESC);
    }
}