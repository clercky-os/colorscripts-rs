// Author: gutterslob
// Source: http://crunchbang.org/forums/viewtopic.php?pid=130590#p130590

use crate::scripts::*;
CREATE_SPRITE!(Monster, "
[0m[30m|               |               |               |               |[0m
   [31m█     █[0m    [30m|[0m    [32m█     █[0m    [30m|[0m    [33m█     █[0m    [30m|[0m    [34m█     █[0m    [30m|[0m    [35m█     █[0m    [30m|[0m    [36m█     █[0m
   [31m███████[0m    [30m|[0m    [32m███████[0m    [30m|[0m    [33m███████[0m    [30m|[0m    [34m███████[0m    [30m|[0m    [35m███████[0m    [30m|[0m    [36m███████[0m
 [31m███[1m[41m██[0m[31m█[1m[41m██[0m[31m███[0m  [30m|[0m  [32m███[1m[42m██[0m[32m█[1m[42m██[0m[32m███[0m  [30m|[0m  [33m███[1m[43m██[0m[33m█[1m[43m██[0m[33m███[0m  [30m|[0m  [34m███[1m[44m██[0m[34m█[1m[44m██[0m[34m███[0m  [30m|[0m  [35m███[1m[45m██[0m[35m█[1m[45m██[0m[35m███[0m  [30m|[0m  [36m███[1m[46m██[0m[36m█[1m[46m██[0m[36m███[0m
  [31m████[1m[41m█[0m[31m████[0m   [30m|[0m   [32m████[1m[42m█[0m[32m████[0m   [30m|[0m   [33m████[1m[43m█[0m[33m████[0m   [30m|[0m   [34m████[1m[44m█[0m[34m████[0m   [30m|[0m   [35m████[1m[45m█[0m[35m████[0m   [30m|[0m   [36m████[1m[46m█[0m[36m████[0m
  [31m█ █ [1m█[0m [31m█ █[0m   [30m|[0m   [32m█ █ [1m█[0m [32m█ █[0m   [30m|[0m   [33m█ █ [1m█[0m [33m█ █[0m   [30m|[0m   [34m█ █ [1m█[0m [34m█ █[0m   [30m|[0m   [35m█ █ [1m█[0m [35m█ █[0m   [30m|[0m   [36m█ █ [1m█[0m [36m█ █[0m
    [31m█   █[0m     [30m|[0m     [32m█   █[0m     [30m|[0m     [33m█   █[0m     [30m|[0m     [34m█   █[0m     [30m|[0m     [35m█   █[0m     [30m|[0m     [36m█   █[0m
                             [30m|               |               |               |               |[0m
");
