const SKULL: &str = "
[1m[37m    ▄▄▄[0m
[1m[37m ▄█████▄▄ [0m
[1m[37m███[46m▀▀▀▀[40m▀[46m▀[40m▀[46m▀[0m
[1m[37m███[46m▄   [22m[30m▀ ▀[0m[36m▀[0m
[1m[37m ▄[46m  [0m[0m[37m█████▄ [22m[31m█▄[0m
[22m[31m▀▀[0m[1m[41m[37m▄[46m▄   [41m▄▄▄[0m[22m[31m▀██▀[0m
[1m[37m ██▀▀▀██▀  [22m[31m▀[0m
[1m[37m ▀▀▀▀ ▀▀▀▀[0m
";

pub struct Elfman {}

impl crate::scripts::Script for Elfman {
    fn get_name(&self) -> String {
        "Elfman".to_string()
    }

    // Start script
    fn exec(&self) {
        println!("{}", SKULL);
    }

}
