macro_rules! CREATE_SPRITE {
    ($name:ident, $value:expr) => {
        pub struct $name {}
        impl Script for $name {
            fn get_name(&self) -> String {
                stringify!($name).to_string()
            }
            fn exec(&self) {
                println!("{}", $value);
            }
        }
    };
}

#[cfg(feature = "script-alpha")]
pub mod alpha;
#[cfg(feature = "script-arch")]
pub mod arch;
#[cfg(feature = "script-bars")]
pub mod bars;
#[cfg(feature = "script-default")]
pub mod default;
#[cfg(feature = "script-elfman")]
pub mod elfman;
#[cfg(feature = "script-monster")]
pub mod monster;
#[cfg(feature = "script-poke-001")]
pub mod poke_001;
#[cfg(feature = "script-poke-047")]
pub mod poke_047;
#[cfg(feature = "script-poke-064")]
pub mod poke_064;
#[cfg(feature = "script-poke-203")]
pub mod poke_203;
#[cfg(feature = "script-pukeskull")]
pub mod pukeskull;
#[cfg(feature = "script-space-invaders")]
pub mod space_invaders;

pub(crate) trait Script {
    fn get_name(&self) -> String;
    fn exec(&self);
}
