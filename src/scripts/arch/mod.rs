// Author: Ivo
// Source: http://crunchbang.org/forums/viewtopic.php?pid=237794#p237794
// ANSI Color -- use these variables to easily have different color
// and format output. Make sure to output the reset sequence after 
// colors (f = foreground, b = background), and use the 'off'
// feature for anything you turn on.

const ESC: &str = "[";

pub struct Arch {}

impl crate::scripts::Script for Arch {
    fn get_name(&self) -> String {
        "Arch".to_string()
    }

    fn exec(&self) {
        let boldon = format!("{}{}", ESC, "1m");
        let redf = format!("{}{}", ESC, "31m");
        let reset = format!("{}{}", ESC, "0m");
        let greenf =  format!("{}{}", ESC, "32m");
        let yellowf =  format!("{}{}", ESC, "33m");
        let bluef =  format!("{}{}", ESC, "34m");
        let purplef =  format!("{}{}", ESC, "35m");
        let cyanf = format!("{}{}", ESC, "36m");

        println!("
{}{}        ■      {}{}        ■      {}{}        ■     {}{}         ■       {}{}       ■      {}{}        ■   {}
{}{}       ■■■     {}{}       ■■■     {}{}       ■■■    {}{}        ■■■      {}{}      ■■■     {}{}       ■■■  {}
{}{}      ■■■■■    {}{}      ■■■■■    {}{}      ■■■■■   {}{}       ■■■■■     {}{}     ■■■■■    {}{}      ■■■■■ {}
{}     ■(   )■   {}     ■(   )■   {}     ■(   )■   {}     ■(   )■    {}    ■(   )■   {}     ■(   )■   {}
{}    ■■■■ ■■■■  {}    ■■■■ ■■■■  {}    ■■■■ ■■■■  {}    ■■■■ ■■■■   {}   ■■■■ ■■■■  {}    ■■■■ ■■■■  {}
{}   ■■       ■■ {}   ■■       ■■ {}   ■■       ■■ {}   ■■       ■■  {}  ■■       ■■ {}   ■■       ■■ {}
             ",
            boldon, redf, boldon, greenf, boldon, yellowf, boldon, bluef, boldon, purplef, boldon, cyanf, reset,
            boldon, redf, boldon, greenf, boldon, yellowf, boldon, bluef, boldon, purplef, boldon, cyanf, reset,
            boldon, redf, boldon, greenf, boldon, yellowf, boldon, bluef, boldon, purplef, boldon, cyanf, reset,
            redf, greenf, yellowf, bluef, purplef, cyanf, reset,
            redf, greenf, yellowf, bluef, purplef, cyanf, reset,
            redf, greenf, yellowf, bluef, purplef, cyanf, reset,
        );
    }
}