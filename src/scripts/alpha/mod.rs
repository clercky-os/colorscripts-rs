// ANSI Color -- use these variables to easily have different color
// and format output. Make sure to output the reset sequence after 
// colors (f = foreground, b = background), and use the 'off'
//  feature for anything you turn on.
//  Author: Ivo
//  Source: http://crunchbang.org/forums/viewtopic.php?pid=134749#p134749

const ESC: &str = "[";

pub struct Alpha {}

impl crate::scripts::Script for Alpha {
    fn get_name(&self) -> String {"Alpha".to_string()}
    
    fn exec(&self) {
        let boldon = format!("{}{}", ESC, "1m");
        let redf = format!("{}{}", ESC, "31m");
        let reset = format!("{}{}", ESC, "0m");
        let greenf =  format!("{}{}", ESC, "32m");
        let yellowf =  format!("{}{}", ESC, "33m");
        let bluef =  format!("{}{}", ESC, "34m");
        let purplef =  format!("{}{}", ESC, "35m");
        let cyanf = format!("{}{}", ESC, "36m");

        println!("
{}{}   ██████  {} {}{}██████     {}{}{}██████{} {}{}██████  {} {}{}  ██████{} {}{}  ███████{}
{}{}   ████████{} {}{}██    ██ {}{}{}██{}       {}{}██    ██{} {}{}██████{}   {}{}█████████{}
{}   ██  ████{} {}██  ████ {}{}████{}     {}████  ██{} {}████    {} {}█████    {}
{}   ██    ██{} {}██████   {}{}████████{} {}██████  {} {}████████{} {}██       {} 
    ",
        boldon, redf, reset, boldon, greenf, reset, boldon, yellowf, reset, boldon,bluef, reset, boldon,purplef, reset, boldon, cyanf, reset,
        boldon, redf, reset, boldon, greenf, reset, boldon, yellowf, reset, boldon, bluef, reset, boldon, purplef, reset, boldon, cyanf, reset,
       /* boldon, purplef, reset, boldon, cyanf, reset,*/
        redf, reset, greenf, reset,yellowf, reset, bluef, reset, purplef, reset, cyanf, reset,
        redf, reset, greenf, reset, yellowf, reset, bluef, reset, purplef, reset, cyanf, reset);

    }
}
