mod scripts;

use structopt::StructOpt;

#[derive(StructOpt)]
struct Cli {
    #[structopt(short = "e", long = "execute")]
    scriptname: Option<String>,
    #[structopt(short = "r", long = "random")]
    random: bool,
    #[structopt(short = "p", long = "print-all")]
    print_all: bool,
}

type ScriptList = Vec<Box<dyn scripts::Script>>;

macro_rules! COND_SPRITE {
    ($feature_name:expr, $script_array:ident => $obj:expr) => {
        #[cfg(feature = $feature_name)]
        ($script_array).push(Box::new($obj));
    };
}

fn main() {
    #[allow(unused_mut)]
    let mut scripts: ScriptList = vec![];
    COND_SPRITE!("script-alpha", scripts => scripts::alpha::Alpha {});
    COND_SPRITE!("script-arch", scripts => scripts::arch::Arch {});
    COND_SPRITE!("script-bars", scripts => scripts::bars::Bars {});
    COND_SPRITE!("script-default", scripts => scripts::default::Default {});
    COND_SPRITE!("script-pukeskull", scripts => scripts::pukeskull::Pukeskull {});
    COND_SPRITE!("script-elfman", scripts => scripts::elfman::Elfman {});
    COND_SPRITE!("script-monster", scripts => scripts::monster::Monster {});
    COND_SPRITE!("script-space-invaders", scripts => scripts::space_invaders::SpaceInvaders {});
    COND_SPRITE!("script-poke-001", scripts => scripts::poke_001::Poke001 {});
    COND_SPRITE!("script-poke-047", scripts => scripts::poke_047::Poke047 {});
    COND_SPRITE!("script-poke-064", scripts => scripts::poke_064::Poke064 {});
    COND_SPRITE!("script-poke-203", scripts => scripts::poke_203::Poke203 {});

    let args = Cli::from_args();

    if args.print_all {
        print_all_possibilities(&scripts);
        return; // terminate execution
    }

    if args.random {
        // choose random
        let randint = fastrand::usize(..scripts.len());
        run_script(scripts[randint].get_name().as_str(), &scripts);
    } else {
        // safe to just unwrap since StructOpt garantees there is one
        run_script(args.scriptname.unwrap().as_str(), &scripts);
    }
}

fn run_script(script: &str, scripts: &ScriptList) {
    let script = scripts
        .iter()
        .filter(|i| i.get_name().to_ascii_lowercase() == script.to_ascii_lowercase())
        .next();
    if let Some(s) = script {
        print!("{}\n", s.get_name());
        s.exec();
        print!("[0m");
    } else {
        eprintln!("Scripts not found");
        print_all_possibilities(scripts);
    }
}

fn print_all_possibilities(scripts: &ScriptList) {
    let script_list: String = scripts
        .iter()
        .clone()
        .map(|i| (*i).get_name())
        .collect::<Vec<String>>()
        .join("\n\t");
    println!("Possible values are: \n\t{}", script_list);
}
